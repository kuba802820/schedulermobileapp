import { TouchableOpacity, StyleSheet } from 'react-native';
import { useModal } from '../../Hooks/useModal';
import React, { useContext } from 'react';
import Add from '../../assets/svg/add.svg';
import { ThemeData } from '../../theme';
import { ThemeContext } from '../../Components/StackPages/StackPages';
const AddButton = () => {
    const { setOpen } = useModal();
    const Theme: ThemeData = useContext(ThemeContext);
    return (
        <TouchableOpacity
            onPress={() => {
                setOpen(true);
            }}
            style={[styles.buttonAdd, { backgroundColor: Theme.secondary }]}>
            <Add width={40} height={40} />
        </TouchableOpacity>
    );
};
const styles = StyleSheet.create({
    buttonAdd: {
        display: 'flex',
        height: '100%',
        width: '100%',
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
    },
});
export default AddButton;
