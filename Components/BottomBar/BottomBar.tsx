import React from 'react';
import { StyleSheet, View } from 'react-native';
import AddButton from './AddButton';
import ModalUI from '../Modal/Modal';
const BottomBar = () => {
    return (
        <View style={styles.container}>
            <ModalUI />
            <AddButton />
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        bottom: 0,
        right: 0,
        width: '100%',
        height: '10%',
        position: 'absolute',
        display: 'flex',
        flexDirection: 'row',
    },
});
export default BottomBar;
