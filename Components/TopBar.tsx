import React, { useContext } from 'react';
import { View, StyleSheet, Picker } from 'react-native';
import { useCalendar } from '../Hooks/useCallendarData';
import PickerUI from './UI/Picker';
import { ThemeData } from '../theme';
import { ThemeContext } from './StackPages/StackPages';

const TopBar = () => {
    const { setMonth, setYear, month, year } = useCalendar();
    const Theme: ThemeData = useContext(ThemeContext);
    const yearData = [
        2020,
        2021,
        2022,
        2023,
        2024,
        2025,
        2026,
        2027,
        2028,
        2029,
        2030,
        2031,
        2032,
        2033,
        2034,
        2035,
        2036,
        2037,
        2038,
        2039,
        2040,
        2041,
        2042,
        2043,
        2044,
        2045,
        2046,
        2047,
        2048,
        2049,
        2050,
    ];
    const monthData = [
        'Styczeń',
        'Luty',
        'Marzec',
        'Kwiecień',
        'Maj',
        'Czerwiec',
        'Lipiec',
        'Sierpień',
        'Wrzesień',
        'Październik',
        'Listopad',
        'Grudzień',
    ];
    return (
        <View style={[styles.container, { backgroundColor: Theme.secondary }]}>
            <PickerUI
                defaultLabel={`Wybrany: ${year.toString()}`}
                onChange={(e) => {
                    setYear(e);
                }}>
                {yearData.map((v) => {
                    return (
                        <Picker.Item label={v.toString()} key={v} value={v} />
                    );
                })}
            </PickerUI>
            <PickerUI
                defaultLabel={`Wybrany: ${monthData[month]}`}
                onChange={(e) => {
                    setMonth(e);
                }}>
                {monthData.map((v, i) => {
                    return (
                        <Picker.Item label={v.toString()} key={v} value={i} />
                    );
                })}
            </PickerUI>
        </View>
    );
};

const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: 100,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
    },
    logoutBtn: {
        padding: 20,
    },
});
export default TopBar;
