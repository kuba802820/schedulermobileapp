import { TouchableOpacity, StyleSheet } from 'react-native';
import React from 'react';
import Dark from '../../assets/svg/dark.svg';
import Light from '../../assets/svg/light.svg';
import { useTheme } from '../../Hooks/useTheme';

const ThemeSwitch = () => {
    const { Theme, toogleTheme } = useTheme();
    return (
        <TouchableOpacity
            onPress={() => {
                toogleTheme();
            }}>
            {Theme === 'light' ? (
                <Dark
                    width={30}
                    height={30}
                    fill="#000"
                    style={styles.themeIcon}
                />
            ) : (
                <Light
                    width={35}
                    height={35}
                    fill="#FFF"
                    style={styles.themeIcon}
                />
            )}
        </TouchableOpacity>
    );
};
const styles = StyleSheet.create({
    themeIcon: {
        marginRight: 10,
    },
});
export default ThemeSwitch;
