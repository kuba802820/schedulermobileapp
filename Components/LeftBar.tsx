import React from 'react';
import { StyleSheet, View, ScrollView } from 'react-native';
import { Calendar } from 'calendar';
import { useCalendar } from '../Hooks/useCallendarData';
import DayItem from './DayItem/DayItem';
const LeftBar = () => {
    const cal = new Calendar(1);
    const callendarStore = useCalendar();
    const month: any[] = cal.monthDays(
        callendarStore.year,
        callendarStore.month
    );
    const monthFlatedArray: number[] = month.flatMap((x) => x);
    return (
        <View style={styles.container}>
            <ScrollView showsVerticalScrollIndicator={false}>
                {monthFlatedArray.map((v: number, i: number) => {
                    return <DayItem value={v} idx={i} key={i} />;
                })}
            </ScrollView>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        width: '25%',
        zIndex: -1,
        height: '83%',
        backgroundColor: '#ECEFF1',
    },
});
export default LeftBar;
