import { TouchableOpacity, View, Text, StyleSheet } from 'react-native';
import React, { useContext } from 'react';
import { IData } from '../../Hooks/useEvent';
import { ThemeData } from 'theme';
import { ThemeContext } from '../../Components/StackPages/StackPages';
interface IProps {
    displayData: (v: IData) => void;
    value: IData;
}
const EventItem = ({ displayData, value }: IProps) => {
    const Theme: ThemeData = useContext(ThemeContext);
    return (
        <TouchableOpacity
            style={[
                styles.event,
                {
                    backgroundColor: Theme.backgroundInfo,
                },
            ]}
            key={value.id}
            onPress={() => displayData(value)}>
            <View
                style={[
                    styles.eventIndicator,
                    { backgroundColor: value.color },
                ]}
            />
            <Text style={[styles.textStyle, { color: Theme.textColor }]}>
                {value.type}
            </Text>
            <Text style={[styles.textStyle, { color: Theme.textColor }]}>
                {value.schoolsubject!.length > 39
                    ? `${value.schoolsubject!.substr(0, 39)}...`
                    : value.schoolsubject}
            </Text>
        </TouchableOpacity>
    );
};
const styles = StyleSheet.create({
    event: {
        width: '100%',
        height: 90,
        borderBottomColor: '#E0E0E0',
        borderBottomWidth: 1,
        padding: 10,
    },
    eventIndicator: {
        borderRadius: 1000,
        width: 20,
        height: 20,
    },
    textStyle: {
        fontFamily: 'Roboto-Regular',
    },
});
export default EventItem;
