import { View, TouchableOpacity, StyleSheet, Text } from 'react-native';
import { useSelectDay } from '../../Hooks/useSelectDay';
import React, { useEffect, useContext } from 'react';
import { ThemeContext } from '../../Components/StackPages/StackPages';
import { ThemeData } from 'theme';
interface IProps {
    value: number;
    idx: number;
}

const DayItem = ({ value, idx }: IProps) => {
    const { day, setDay } = useSelectDay();
    const Theme: ThemeData = useContext(ThemeContext);
    useEffect(() => {
        const actualDay = new Date().getDate();
        setDay(actualDay);
    }, []);
    return (
        <View key={idx}>
            {value !== 0 && (
                <TouchableOpacity
                    onPress={() => {
                        setDay(value);
                    }}
                    style={[
                        styles.buttonDay,
                        { backgroundColor: Theme.primary },
                        day === value && { backgroundColor: Theme.active },
                    ]}>
                    <Text style={styles.textButton}>{value}</Text>
                </TouchableOpacity>
            )}
        </View>
    );
};
const styles = StyleSheet.create({
    textButton: {
        color: '#fff',
        fontSize: 15,
    },
    buttonDay: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        borderBottomColor: '#ffffff',
        borderBottomWidth: 1,
        height: 90,
    },
});
export default DayItem;
