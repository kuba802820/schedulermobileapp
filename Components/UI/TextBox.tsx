import {
    TextInput,
    Dimensions,
    StyleSheet,
    NativeSyntheticEvent,
    TextInputChangeEventData,
} from 'react-native';
import React, { useContext } from 'react';
import { ThemeData } from '../../theme';
import { ThemeContext } from '../../Components/StackPages/StackPages';
interface IProps {
    placeholder?: string;
    value?: string;
    method?: (e: NativeSyntheticEvent<TextInputChangeEventData>) => void;
    onFocus?: (e: any) => void;
    fullWidth?: boolean;
    multiline?: boolean;
    numOfLine?: number;
}
const TextBox = ({
    placeholder,
    value,
    method,
    onFocus,
    fullWidth,
    multiline,
    numOfLine,
}: IProps) => {
    const Theme: ThemeData = useContext(ThemeContext);
    return (
        <TextInput
            placeholder={placeholder}
            value={value}
            placeholderTextColor={Theme.textColor}
            onChange={method}
            onFocus={onFocus}
            multiline={multiline}
            numberOfLines={numOfLine}
            style={[
                styles.textBoxStyle,
                { color: Theme.textColor },
                fullWidth || false ? styles.fullWidth : styles.notFullWidth,
                multiline && styles.textTopLeftCorner,
            ]}
        />
    );
};
const styles = StyleSheet.create({
    textBoxStyle: {
        padding: 10,
        borderColor: 'gray',
        borderWidth: 1,
        marginTop: 5,
        marginBottom: 5,
        borderRadius: 10,
    },
    textTopLeftCorner: {
        textAlignVertical: 'top',
    },
    fullWidth: {
        width: '100%',
    },
    notFullWidth: {
        width:
            Dimensions.get('window').width -
            Dimensions.get('window').width * 0.1,
    },
});
export default TextBox;
