import { Picker, StyleSheet } from 'react-native';
import React, { ReactNode, useContext } from 'react';
import { ThemeData } from '../../theme';
import { ThemeContext } from '../../Components/StackPages/StackPages';
interface IProps {
    defaultLabel?: string;
    onChange: (e: any) => void;
    children?: ReactNode;
}
const PickerUI = ({ defaultLabel, children, onChange }: IProps) => {
    const Theme: ThemeData = useContext(ThemeContext);
    return (
        <Picker
            itemStyle={{ textAlign: 'center' }}
            style={[
                styles.pickerStyle,
                {
                    backgroundColor: Theme.pickerColor,
                    color: Theme.textColor,
                },
            ]}
            onValueChange={onChange}>
            <Picker.Item label={defaultLabel ?? ''} />
            {children}
        </Picker>
    );
};
const styles = StyleSheet.create({
    pickerStyle: {
        height: 60,
        width: '50%',
    },
});
export default PickerUI;
