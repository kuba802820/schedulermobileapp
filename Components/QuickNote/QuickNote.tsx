import React, { useState, useEffect, useContext } from 'react';
import { View, StyleSheet, Button } from 'react-native';
import TextBox from '../../Components/UI/TextBox';
import { useSaveNote } from '../../Hooks/useSaveNote';
import Snackbar from 'react-native-snackbar';
import { ThemeData } from '../../theme';
import { ThemeContext } from '../../Components/StackPages/StackPages';
const QuickNote = ({ id }: { id?: number }) => {
    const { getNote, saveNote } = useSaveNote();
    const [localNote, setLocalNote] = useState<string | undefined>('');
    const Theme: ThemeData = useContext(ThemeContext);

    useEffect(() => {
        const getNoteFromStorage = async () => {
            const note = await getNote(id);
            setLocalNote(note);
        };
        getNoteFromStorage();
    }, []);
    return (
        <>
            <View
                style={[
                    styles.textAreaContainer,
                    { backgroundColor: Theme.backgroundColor },
                ]}>
                <TextBox
                    fullWidth={true}
                    multiline={true}
                    numOfLine={10}
                    placeholder="Wpisz notatkę"
                    value={localNote}
                    method={(e) => {
                        setLocalNote(e.nativeEvent.text);
                    }}
                />
                <View style={styles.noteSaveBtn}>
                    <Button
                        color={Theme.primary}
                        title="Zapisz"
                        onPress={() => {
                            saveNote(localNote, id);
                            Snackbar.show({
                                text: 'Pomyślnie zapisano notatkę',
                                duration: Snackbar.LENGTH_SHORT,
                            });
                        }}
                    />
                </View>
            </View>
        </>
    );
};
const styles = StyleSheet.create({
    textAreaContainer: {
        padding: 5,
        height: '50%',
    },
    textArea: {
        height: 150,
    },
    noteSaveBtn: {
        marginTop: 5,
    },
});
export default QuickNote;
