import React, { useContext } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import Type from '../../assets/svg/type.svg';
import SchoolSubject from '../../assets/svg/schoolSubject.svg';
import Description from '../../assets/svg/description.svg';
import { IData } from 'Hooks/useEvent';
import { ThemeData } from '../../theme';
import { ThemeContext } from '../../Components/StackPages/StackPages';
const EventViewer = ({ type, schoolsubject, description, color }: IData) => {
    const Theme: ThemeData = useContext(ThemeContext);
    return (
        <View style={styles.container}>
            <View
                style={[
                    styles.itemEvent,
                    { backgroundColor: Theme.primary, borderLeftWidth: 0 },
                ]}>
                <Type
                    width={90}
                    height={90}
                    opacity={0.05}
                    style={styles.iconItem}
                />
                <View
                    style={[styles.typeIndicator, { backgroundColor: color }]}
                />
                <Text style={styles.textStyle}>{type}</Text>
            </View>
            <View
                style={[styles.itemEvent, { backgroundColor: Theme.primary }]}>
                <SchoolSubject
                    width={90}
                    height={90}
                    opacity={0.05}
                    style={styles.iconItem}
                />
                <Text style={styles.textStyle}>{schoolsubject}</Text>
            </View>
            <View
                style={[
                    styles.itemFullWidth,
                    { backgroundColor: Theme.primary, borderBottomWidth: 0 },
                ]}>
                <Description
                    width={90}
                    height={90}
                    opacity={0.05}
                    style={styles.iconItem}
                />
                <Text style={styles.textStyle}>{description}</Text>
            </View>
        </View>
    );
};
const styles = StyleSheet.create({
    container: {
        width: '100%',
        height: '50%',
        display: 'flex',
        flexDirection: 'row',
        flexWrap: 'wrap',
        alignItems: 'center',
    },
    iconItem: {
        position: 'absolute',
    },
    textStyle: {
        color: '#fff',
        textAlign: 'center',
        fontSize: 20,
        fontFamily: 'Lato-Light',
    },
    itemEvent: {
        width: '50%',
        height: '50%',
        borderLeftWidth: 0.5,
        borderLeftColor: '#fff',
        backgroundColor: '#707fcf',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        padding: 5,
    },
    itemFullWidth: {
        width: '100%',
        height: '50%',
        borderTopWidth: 0.5,
        borderTopColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center',
    },
    typeIndicator: {
        width: 35,
        height: 35,
        borderBottomRightRadius: 30,
        opacity: 0.7,
        position: 'absolute',
        left: 0,
        top: 0,
    },
});
export default EventViewer;
