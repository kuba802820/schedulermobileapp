import { useModal } from '../../Hooks/useModal';
import { View, Button, Text, Modal, Picker, StyleSheet } from 'react-native';
import { Cattegories, Subjects } from './Cattegories';
import PickerUI from '../UI/Picker';
import TextBox from '../UI/TextBox';
import React, { useState, useContext } from 'react';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import { useSubmitEvent } from '../../Hooks/useSubmitEvent';
import { ThemeData } from '../../theme';
import { ThemeContext } from '../../Components/StackPages/StackPages';
export interface IForm {
    description?: NonNullable<string>;
    cattegories: {
        name?: NonNullable<string>;
        color?: NonNullable<string>;
    };
    subjects?: NonNullable<string>;
    selectedDate?: NonNullable<string>;
}
type IResult = string;
const ModalUI = () => {
    const Theme: ThemeData = useContext(ThemeContext);
    const { isOpen, setOpen } = useModal();
    const [isPickerOpen, setPickerState] = useState(false);
    const [formData, setFormData] = useState<IForm>({
        description: '',
        cattegories: {
            name: '',
            color: '',
        },
        subjects: '',
        selectedDate: '',
    });
    const { onSubmit } = useSubmitEvent(formData);
    const formatDate = (date: Date) =>
        date.toISOString().substring(0, date.toISOString().indexOf('T'));
    return (
        <Modal animationType="slide" transparent={false} visible={isOpen}>
            <View
                style={[
                    styles.root,
                    { backgroundColor: Theme.backgroundColor },
                ]}>
                <Text
                    style={[
                        styles.deadLinePicker,
                        { backgroundColor: Theme.primary },
                    ]}
                    onPress={() => {
                        setPickerState(!isPickerOpen);
                    }}>
                    👉 Wybierz termin: {formData.selectedDate} 👈
                </Text>
                <DateTimePickerModal
                    isVisible={isPickerOpen}
                    mode="date"
                    maximumDate={new Date(2050, 1, 1)}
                    minimumDate={new Date(2020, 1, 1)}
                    onConfirm={(e) => {
                        setPickerState(false);
                        setFormData({
                            ...formData,
                            selectedDate: formatDate(e),
                        });
                    }}
                    onCancel={() => {
                        setPickerState(false);
                    }}
                />
                <View style={styles.flexCenter}>
                    <TextBox
                        placeholder="Opis ..."
                        value={formData.description}
                        method={({ nativeEvent }) => {
                            setFormData({
                                ...formData,
                                description: nativeEvent.text,
                            });
                        }}
                    />
                </View>
                <View style={styles.pickersRoot}>
                    <PickerUI
                        defaultLabel={
                            formData.cattegories.name!.length > 0
                                ? `${formData.cattegories.name} ▼`
                                : 'Kategoria ▼'
                        }
                        onChange={(e) =>
                            setFormData({
                                ...formData,
                                cattegories: {
                                    name: e.name,
                                    color: e.color,
                                },
                            })
                        }>
                        {Cattegories.map((v) => {
                            return (
                                <Picker.Item
                                    label={v.type.toString()}
                                    key={v.id}
                                    value={{ name: v.type, color: v.color }}
                                />
                            );
                        })}
                    </PickerUI>
                    <PickerUI
                        defaultLabel={
                            formData.subjects!.length > 0
                                ? `${formData.subjects} ▼`
                                : 'Przedmiot ▼'
                        }
                        onChange={(e) =>
                            setFormData({ ...formData, subjects: e })
                        }>
                        {Subjects.map((v) => {
                            return (
                                <Picker.Item
                                    label={v.name.toString()}
                                    key={v.id}
                                    value={v.name}
                                />
                            );
                        })}
                    </PickerUI>
                </View>

                <Button
                    color={Theme.primary}
                    title="Dodaj ＋"
                    onPress={onSubmit}
                />
                <View style={{ marginTop: 20 }}>
                    <Button
                        color={Theme.primary}
                        title="Zamknij ╳"
                        onPress={() => {
                            console.log('PRESS');
                            setFormData({
                                description: '',
                                cattegories: {
                                    name: '',
                                    color: '',
                                },
                                subjects: '',
                            });
                            setOpen(false);
                        }}
                    />
                </View>
            </View>
        </Modal>
    );
};

const styles = StyleSheet.create({
    root: {
        height: '100%',
    },
    deadLinePicker: {
        fontSize: 20,
        backgroundColor: '#455399',
        padding: 10,
        color: '#fff',
        textAlign: 'center',
        marginBottom: 20,
    },
    flexCenter: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
    },
    pickersRoot: {
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        marginTop: 10,
        marginBottom: 10,
        flexDirection: 'row',
    },
});
export default ModalUI;
