import React, { createContext, useEffect } from 'react';
import { Stack, Scene, Actions, Router } from 'react-native-router-flux';
import Schedule from '../../Views/Schedule';
import ThemeSwitch from '../../Components/ThemeSwitch/ThemeSwitch';
import Event from '../../Views/Event';
import Home from '../../Views/Home';
import { useTheme } from '../../Hooks/useTheme';
import { StyleSheet } from 'react-native';
import { theme } from '../../theme';
export const ThemeContext = createContext({});

const StackPages = () => {
    const { Theme } = useTheme();
    return (
        <ThemeContext.Provider
            value={Theme === 'dark' ? theme.dark : theme.light}>
            <Router
                titleStyle={[
                    Theme === 'dark' ? styles.AppBarDark : styles.AppBarWhite,
                ]}
                navigationBarStyle={[
                    Theme === 'dark' ? styles.AppBarDark : styles.AppBarWhite,
                ]}>
                <Stack key="root">
                    <Scene
                        key="home"
                        component={Schedule}
                        title="Terminarz"
                        renderRightButton={() => <ThemeSwitch />}
                        renderLeftButton={null}
                    />
                    <Scene
                        key="event"
                        headerBackTitle={'Wróć'}
                        component={Event}
                        title="Wydarzenie"
                    />
                    <Scene
                        key="login"
                        component={Home}
                        initial
                        onEnter={() => {
                            Actions.home();
                        }}
                    />
                </Stack>
            </Router>
        </ThemeContext.Provider>
    );
};

const styles = StyleSheet.create({
    AppBarDark: {
        backgroundColor: '#28353d',
        color: '#fff',
    },
    AppBarWhite: {
        backgroundColor: '#fff',
        color: '#000',
    },
});

export default StackPages;
