import React, { useContext } from 'react';
import {
    Text,
    View,
    StyleSheet,
    ScrollView,
    ActivityIndicator,
} from 'react-native';
import { Actions } from 'react-native-router-flux';
import { useEvent, IData } from '../Hooks/useEvent';
import EventItem from './EventItem/EventItem';
import { ThemeData } from '../theme';
import { ThemeContext } from './StackPages/StackPages';
const Events = () => {
    const { event } = useEvent();
    const Theme: ThemeData = useContext(ThemeContext);
    const displayData = (data: IData) => {
        Actions.event({ props: data });
    };
    return (
        <View style={styles.Wrapper}>
            {event ? (
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    {event.length > 0 ? (
                        event.map((v) => {
                            return (
                                <EventItem
                                    displayData={() => displayData(v)}
                                    value={v}
                                    key={v.id}
                                />
                            );
                        })
                    ) : (
                        <View
                            style={[
                                styles.event,
                                {
                                    backgroundColor: Theme.backgroundInfo,
                                },
                            ]}>
                            <Text
                                style={[
                                    styles.noEvents,
                                    {
                                        color: Theme.textColor,
                                    },
                                ]}>
                                Brak zapisanych wydarzeń
                            </Text>
                        </View>
                    )}
                </ScrollView>
            ) : (
                <View style={styles.Loading}>
                    <Text style={styles.TextLoading}>Ładowanie</Text>
                    <ActivityIndicator size="large" color="#5C6BC0" />
                </View>
            )}
        </View>
    );
};
const styles = StyleSheet.create({
    event: {
        width: '75%',
        height: 60,
        backgroundColor: '#EEEEEE',
        borderBottomColor: '#E0E0E0',
        borderBottomWidth: 1,
        padding: 10,
    },
    noEvents: {
        textAlign: 'center',
        lineHeight: 40,
    },
    Loading: {
        width: 'auto',
        height: '5%',
        display: 'flex',
        margin: 10,
        flexDirection: 'row',
    },
    TextLoading: {
        fontSize: 20,
        color: '#5C6BC0',
    },
    Wrapper: {
        width: '100%',
        height: '83%',
    },
});
export default Events;
