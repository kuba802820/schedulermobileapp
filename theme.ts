export const theme: TypeOfTheme = {
    dark: {
        primary: '#1a1d1f',
        secondary: '#101214',
        active: '#3a414a',
        backgroundInfo: '#252d36',
        textColor: '#FFF',
        backgroundColor: '#262d33',
        pickerColor: '#2e313b',
    },
    light: {
        primary: '#455399',
        secondary: '#323d73',
        active: '#7E57C2',
        backgroundInfo: '#EEEEEE',
        textColor: '#000',
        backgroundColor: '#dedfe0',
        pickerColor: '#f2f2f2',
    },
};
export type ThemeData = {
    primary?: string;
    secondary?: string;
    active?: string;
    backgroundInfo?: string;
    textColor?: string;
    backgroundColor?: string;
    pickerColor?: string;
};
interface TypeOfTheme {
    dark: ThemeData;
    light: ThemeData;
}
