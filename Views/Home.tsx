import React, { useState, useEffect, useContext } from 'react';
import {
    SafeAreaView,
    StyleSheet,
    Text,
    Button,
    View,
    AsyncStorage,
} from 'react-native';
import { useAuthHook } from '../Hooks/useAuth';
import TextBox from '../Components/UI/TextBox';
import { ThemeData } from '../theme';
import { ThemeContext } from '../Components/StackPages/StackPages';
const Home = () => {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const { registerUser, loginUser } = useAuthHook();
    const Theme: ThemeData = useContext(ThemeContext);

    useEffect(() => {
        const LoginAndSetTheme = async () => {
            await loginUser();
        };
        LoginAndSetTheme();
    }, []);
    return (
        <SafeAreaView
            style={[
                styles.container,
                { backgroundColor: Theme.backgroundColor },
            ]}>
            <Text style={[styles.header, { color: Theme.textColor }]}>
                Kliknij poniższy przycisk aby się autoryzować
            </Text>
            <TextBox
                placeholder="Imie..."
                value={firstName}
                method={(e) => {
                    setFirstName(e.nativeEvent.text);
                }}
                fullWidth={false}
            />
            <TextBox
                placeholder="Nazwisko..."
                value={lastName}
                method={(e) => {
                    setLastName(e.nativeEvent.text);
                }}
                fullWidth={false}
            />
            <View style={styles.authorizeBtn}>
                <Button
                    color={Theme.primary}
                    title="Autoryzuj się"
                    onPress={() => registerUser(firstName, lastName)}
                />
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        paddingTop: 50,
    },
    header: {
        textAlign: 'center',
        fontSize: 20,
        margin: 10,
    },
    authorizeBtn: {
        width: '90%',
        padding: 20,
    },
});
export default Home;
