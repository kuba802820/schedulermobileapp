import React from 'react';
import { IData } from 'Hooks/useEvent';
import EventViewer from '../Components/EventViewer/EventViewer';
import QuickNote from '../Components/QuickNote/QuickNote';

interface IProps {
    navigation: {
        state: {
            params: {
                props: IData;
            };
        };
    };
}
const Event = (data: IProps) => {
    const {
        type,
        schoolsubject,
        description,
        color,
        id,
    } = data.navigation.state.params.props;
    return (
        <>
            <EventViewer
                type={type}
                schoolsubject={schoolsubject}
                description={description}
                color={color}
            />
            <QuickNote id={id} />
        </>
    );
};

export default Event;
