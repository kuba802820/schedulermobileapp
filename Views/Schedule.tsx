import React, { useContext } from 'react';
import { SafeAreaView, View, StyleSheet, StatusBar } from 'react-native';
import TopBar from '../Components/TopBar';
import LeftBar from '../Components/LeftBar';
import Events from '../Components/Events';
import BottomBar from '../Components/BottomBar/BottomBar';
import { useAuthHook } from '../Hooks/useAuth';
import { ThemeData } from '../theme';
import { ThemeContext } from '../Components/StackPages/StackPages';

const Schedule = () => {
    const { isLogin } = useAuthHook();
    const Theme: ThemeData = useContext(ThemeContext);
    return (
        <SafeAreaView
            style={[
                styles.container,
                { backgroundColor: Theme.backgroundColor },
            ]}>
            {isLogin && (
                <>
                    <StatusBar
                        backgroundColor={Theme.primary}
                        barStyle="light-content"
                    />
                    <TopBar />
                    <View style={styles.wrapper}>
                        <LeftBar />
                        <Events />
                    </View>
                    <BottomBar />
                </>
            )}
        </SafeAreaView>
    );
};
const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
        alignItems: 'flex-start',
        justifyContent: 'center',
        paddingTop: 50,
    },
    wrapper: {
        height: '100%',
        display: 'flex',
        flexDirection: 'row',
    },
    pickerMonth: {
        zIndex: 100,
    },
});
export default Schedule;
