import axios from 'axios';

import qs from 'qs';
import { Alert } from 'react-native';
export type Method = 'post' | 'get';
interface IResponse<T> {
    error?: string;
    data: {
        result: T;
    };
}
export const makeRequest = async <T, K>(
    endpoint: string,
    method: Method,
    data?: K,
    additionalsHeader?: {}
) => {
    const res: IResponse<T> = await axios({
        url: `http://212.91.16.181:8000/api/${endpoint}`,
        method,
        headers: {
            'content-type': 'application/x-www-form-urlencoded;charset=utf-8',
            ...additionalsHeader,
        },
        data: qs.stringify(data),
        withCredentials: true,
    }).catch((err) => {
        if (!err.response) {
            Alert.alert('Błąd', `${err}`);
        } else {
            return err.response?.data;
        }
    });
    return res;
};
