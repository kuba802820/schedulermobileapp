import React from 'react';
import { ReusableProvider } from 'reusable';
import StackPages from './Components/StackPages/StackPages';

export default function App() {
    return (
        <ReusableProvider>
            <StackPages />
        </ReusableProvider>
    );
}
