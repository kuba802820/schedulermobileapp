module.exports = {
    bracketSpacing: true,
    jsxBracketSameLine: true,
    singleQuote: true,
    tabWidth: 4,
    semi: true,
    endOfLine: 'auto',
};
