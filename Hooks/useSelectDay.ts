import { useState } from 'react';
import { createStore } from 'reusable';
const useSelectDayHook = () => {
    const date = new Date();
    const [day, setDay] = useState(date.getDay());
    return {
        day,
        setDay,
    };
};
export const useSelectDay = createStore(useSelectDayHook);
