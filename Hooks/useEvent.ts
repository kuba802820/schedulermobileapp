import { useState, useEffect } from 'react';
import { createStore } from 'reusable';
import { makeRequest } from '../Helper/reqAPI';
import { useCalendar } from './useCallendarData';
import { useSelectDay } from './useSelectDay';
import { Alert } from 'react-native';

export interface IData {
    id?: number;
    type?: string;
    isPublish?: boolean;
    eventAuthor?: string;
    createdAt?: string;
    color?: string;
    name?: string;
    day?: number;
    year?: number;
    month?: number;
    schoolsubject?: string;
    description?: string;
}

const useEventHook = () => {
    const { day } = useSelectDay();
    const { month, year } = useCalendar();
    const [event, setEvent] = useState<IData[]>();
    useEffect(() => {
        const fetchData = async () => {
            const res = await makeRequest<IData[], IData>(
                `getevents?day=${day.toString()}&month=${month.toString()}&year=${year.toString()}`,
                'get'
            );
            if (typeof res === 'object') {
                setEvent(res.data?.result);
            } else if (typeof res === 'string') {
                Alert.alert('Błąd', res);
            }
        };
        fetchData();
    }, [day, month, year]);
    return {
        event,
    };
};
export const useEvent = createStore(useEventHook);
