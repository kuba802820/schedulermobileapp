import { useState } from 'react';
import { createStore } from 'reusable';
const useDataCalendar = () => {
    const date = new Date();
    const [month, setMonth] = useState(date.getMonth());
    const [year, setYear] = useState(date.getFullYear());
    return {
        month,
        setMonth,
        year,
        setYear,
    };
};
export const useCalendar = createStore(useDataCalendar);
