import { AsyncStorage } from 'react-native';

export const useSaveNote = () => {
    return {
        saveNote: async (note: string | undefined, noteId?: number) =>
            await AsyncStorage.setItem(`${noteId}`, note ?? 'false'),
        getNote: async (noteId?: number) =>
            (await AsyncStorage.getItem(`${noteId}`)) ?? undefined,
    };
};
