import { Alert } from 'react-native';
import { makeRequest } from '../Helper/reqAPI';
import { IForm } from '../Components/Modal/Modal';
export const useSubmitEvent = (formData: IForm) => {
    return {
        onSubmit: async () => {
            const {
                description,
                cattegories,
                subjects,
                selectedDate,
            } = formData;
            if (description!.length <= 0 || description!.length >= 200) {
                return Alert.alert(
                    'Błąd!',
                    'Pole opis nie spełnia wymagań (max 200 znaki, min. 1 znak)'
                );
            }
            if (cattegories.name!.length <= 0 || subjects!.length <= 0) {
                return Alert.alert(
                    'Błąd!',
                    'Musisz wybrać kategorię i/lub przedmiot'
                );
            }
            if (!selectedDate) {
                return Alert.alert('Błąd', 'Wybierz termin');
            }
            const [year, month, day] = selectedDate.split('-');
            const adjustedMonth = Number(month.replace('0', '')) - 1;
            const res = await makeRequest<string, any>('createEvent', 'post', {
                type: cattegories.name,
                color: cattegories.color,
                name: cattegories.name,
                day: day.replace('0', ''),
                month: adjustedMonth.toString(),
                year: year,
                schoolsubject: subjects,
                description,
            });
            const isError = res.error;
            if (isError !== undefined) {
                return Alert.alert('Błąd', res.error);
            }
            Alert.alert('Sukces', `${res.data.result}`);
        },
    };
};
