import { useState } from 'react';
import { createStore } from 'reusable';
import { AsyncStorage } from 'react-native';
export type Theme = 'light' | 'dark';
const useThemeHook = () => {
    const [Theme, setTheme] = useState<Theme>('light');
    return {
        Theme,
        setTheme,
        toogleTheme: () => {
            setTheme(Theme === 'light' ? 'dark' : 'light');
            AsyncStorage.setItem('theme', Theme === 'light' ? 'dark' : 'light');
        },
        loadTheme: async () => {
            const theme = await AsyncStorage.getItem('theme');
            setTheme(theme as Theme);
        },
    };
};
export const useTheme = createStore(useThemeHook);
