import { useState } from 'react';
import { createStore } from 'reusable';
import { Alert, AsyncStorage } from 'react-native';
import { makeRequest } from '../Helper/reqAPI';
import { getMacAddress } from 'react-native-device-info';
import { useTheme } from '../Hooks/useTheme';

import { Actions } from 'react-native-router-flux';
import { Theme } from './useTheme';
const useAuth = () => {
    const [isLogin, setLogin] = useState(true);
    const { loadTheme } = useTheme();
    return {
        isLogin,
        loginUser: async () => {
            const res = await makeRequest('login', 'post', {
                serialNumber: await getMacAddress(),
            });
            const isError = res?.error ? true : false;
            if (isError) {
                Alert.alert('Błąd', `${res.error}`);
            } else {
                const { result }: { result: boolean } = res.data.result;
                if (result) {
                    setLogin(true);
                    await loadTheme();
                    Actions.home();
                }
            }
        },
        registerUser: async (firstName: string, lastName: string) => {
            if (firstName.length <= 0 && lastName.length <= 0) {
                return Alert.alert('Błąd', 'Imie i nazwisko jest wymagane');
            }
            const res = await makeRequest('register', 'post', {
                firstName,
                lastName,
                serialNumber: await getMacAddress(),
            });
            const isError = res?.error ? true : false;
            if (isError) {
                Alert.alert('Błąd', `${res.error}`);
            } else {
                Alert.alert('Sukces', `${res.data.result}`);
            }
        },
    };
};
export const useAuthHook = createStore(useAuth);
