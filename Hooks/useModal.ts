import { useState } from 'react';
import { createStore } from 'reusable';
const useModalHook = () => {
    const [isOpen, setOpen] = useState(false);
    return {
        isOpen,
        setOpen,
    };
};
export const useModal = createStore(useModalHook);
